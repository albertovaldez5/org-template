- [Inlcuding files for the installer to fetch](#inlcuding-files-for-the-installer-to-fetch)



<a id="inlcuding-files-for-the-installer-to-fetch"></a>

# Inlcuding files for the installer to fetch

Let&rsquo;s make sure to include the `Makefile` in resources.

```shell
cp ../Makefile ../public/resources/Makefile
```

    

Copying .gitignore too.

```shell
cp ../.gitignore ../public/resources/.gitignore
```
