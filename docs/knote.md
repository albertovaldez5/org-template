- [Introduction](#introduction)
- [Download the files](#download-the-files)



<a id="introduction"></a>

# Introduction

Knote is an installer for org-mode template projects. For example: <https://gitlab.com/albertovaldez5/org-template>

The objective is to generate the folder structure either automatically or interactively.


<a id="download-the-files"></a>

# Download the files

```python
import argparse

def main(argc: int, argv: argparse.Namespace):

    if argc == 0:
        print("No arguments.")
        return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a redirecting index.html")
    parser.add_argument(
        "path", metavar="path", type=str, help="path to redirect from index.html"
    )
    args = parser.parse_args()
    main(len(args.__dict__), args)
```

    usage: [-h] path
    : error: the following arguments are required: path
