- [Problem](#problem)
- [Planning](#planning)
- [Resources](#resources)
- [Tests](#tests)
- [Data](#data)
- [Code](#code)
- [Testing](#testing)
- [Reproducing the Process](#reproducing-the-process)
- [Misc Data](#misc-data)



<a id="problem"></a>

# Problem

We want to dynamically construct an HTML webpage using Javascript and then test it with Python by scraping its content.


<a id="planning"></a>

# Planning

We need to use an existing HTML template with a defined `header` and `body` and import our Javascript with `script` elements.

Then we need to write Javascript code to dynamically load data into the page. We will load an array object and create a table with it. This means we need two Javascript files, one will be the data and the other the function to display it as a table.

Finally, we need a Python script that will use a Web Driver to load the webpage in the background, find the first table and print it to the standard output.


<a id="resources"></a>

# Resources

The following paths are of the files we are going to create in this document. Then the elisp code will help us access those paths later on.

```elisp
"../resources/scrapeme.html"
```

```elisp
"../resources/data.js"
```

```elisp
"../resources/index.js"
```

```elisp
"../tests/test_index.py"
```

```elisp
(defun filepath () "" "../resources/scrapeme.html")
(defun datapath () "" "../resources/data.js")
(defun scriptpath () "" "../resources/index.js")
(defun testpath () "" "../tests/test_index.py")
```

<div class="note" id="orgf76d8ec">
<p>
For the sake of being explicit, we will export the elisp source blocks that provide the paths to the files we are going to be working with. I would normally hide them as they are meta properties of the document.
</p>

</div>

This is the base HTML file we are going to use. It contains nothing but an empty table with headers but no rows. We are going to fill the rows dynamically with our script.

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <script src="https://d3js.org/d3.v5.min.js"></script>
</head>
<body>
  <table>
    <thead>
      <tr>
        <th>Weekday</th>
        <th>Date</th>
        <th>High</th>
        <th>Low</th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>
</body>
<script src= "../resources/data.js" ></script>
<script src= "../resources/index.js" > </script>
</html>
```


<a id="tests"></a>

# Tests

We will start writing the test to parse our HTML file just to make sure that we can read the data.

This is the Python script we are going to use for scraping the webpage. It will run the web browser in our local file and then scrape it wil BeautifulSoup. We will find the first table element in the document and quit the browser.

```python
from pathlib import Path
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


def setup_browser():
    """Start a headless Chrome browser."""
    options = Options()
    options.headless = True
    service = Service(ChromeDriverManager().install())
    return webdriver.Chrome(service=service, options=options)


def get_first(target: str, browser, url: str):
    """Find any first element of tag equal to given target."""
    browser.get(url)
    soup = BeautifulSoup(browser.page_source, "html.parser")
    return soup.find(target)


def test_browser():
    """Check if there is a table in the document and print it"""
    filepath = Path.cwd().parent / Path("../resources/scrapeme.html".replace("../", ""))
    browser = setup_browser()
    table = get_first("table", browser, f"file://{filepath}")
    assert table is not None
    print(f"{table}")
    browser.quit()


if __name__ == "__main__":
    test_browser()
```

<table>
<thead>
<tr>
<th>Weekday</th>
<th>Date</th>
<th>High</th>
<th>Low</th>
</tr>
</thead>
<tbody></tbody>
</table>


<a id="data"></a>

# Data

Now that the test works, we need mockup data to add to our webpage. Note that the keys match the order of the headers in the HTML file.

```typescript
var data = [
    {
        weekday: "SUN",
        date: "July 1",
        high: 76,
        low: 63
    }, {
        weekday: "MON",
        date: "July 2",
        high: 77,
        low: 63
    }, {
        weekday: "TUE",
        date: "July 3",
        high: 77,
        low: 63
    }, {
        weekday: "WED",
        date: "July 4",
        high: 81,
        low: 65
    }, {
        weekday: "THU",
        date: "July 5",
        high: 87,
        low: 68
    }, {
        weekday: "FRI",
        date: "July 6",
        high: 91,
        low: 71
    }, {
        weekday: "SAT",
        date: "July 7",
        high: 91,
        low: 72
    }
];
```

<div class="note" id="orgc613d13">
<p>
We are using Typescript instead of Javascript and exporting the results as files to the paths we set before. This is not necessary but it has a few workflow benefits, even if we are not adding explicit types yet.
</p>

</div>


<a id="code"></a>

# Code

Now that we have both data and tests done, we can start writing our program. We need to select the body of the table, iterate over our `data` and add each of the items to the table body as a new row.

We will need to use `forEach` twice, first iterate over each item in data, and a second time for each value of given item.

```typescript
let tbody = d3.select("tbody");
// Console.log the weather data from data.js
console.log(data);
// Append new rows to the table
data.forEach((weatherReport) => {
  let row = tbody.append("tr");
  Object.values(weatherReport).forEach((value) => row.append("td").text(value));
});
```


<a id="testing"></a>

# Testing

Now that the program is done, we can run our test with Python.

```shell
python "../tests/test_index.py"
```

<table>
<thead>
<tr>
<th>Weekday</th>
<th>Date</th>
<th>High</th>
<th>Low</th>
</tr>
</thead>
<tbody><tr><td>SUN</td><td>July 1</td><td>76</td><td>63</td></tr><tr><td>MON</td><td>July 2</td><td>77</td><td>63</td></tr><tr><td>TUE</td><td>July 3</td><td>77</td><td>63</td></tr><tr><td>WED</td><td>July 4</td><td>81</td><td>65</td></tr><tr><td>THU</td><td>July 5</td><td>87</td><td>68</td></tr><tr><td>FRI</td><td>July 6</td><td>91</td><td>71</td></tr><tr><td>SAT</td><td>July 7</td><td>91</td><td>72</td></tr></tbody>
</table>

Alternatively, we can use `pytest` in larger codebases.

```shell
pytest ../tests
```

    ============================= test session starts ==============================
    platform darwin -- Python 3.7.13, pytest-7.1.2, pluggy-1.0.0
    rootdir: /Users/albertovaldez/org-template
    collected 1 item
    
    ../tests/test_index.py .                                                 [100%]
    
    ============================== 1 passed in 2.13s ===============================


<a id="reproducing-the-process"></a>

# Reproducing the Process

Because we want to make sure the process works any time we go over it, we are going to remove all the files we generated from this document.

We must make sure we run this document from top to bottom so all the steps are completed before reaching this final step. This means that we will share the project without the final result included, as the results will be removed at the end. However, we could start with this step before running any other code, very much like a `Makefile`&rsquo;s `make clean` command.

```shell
touch "../resources/scrapeme.html" "../resources/data.js" "../resources/index.js" "../tests/test_index.py"
rm "../resources/scrapeme.html" "../resources/data.js" "../resources/index.js" "../tests/test_index.py"
```

We have to make sure that we are triggering tangling for at least this file from our publish.el file. Otherwise, if we are publishing to multiple formats we may end up running the clean command twice and the tangling just once.

Now we can publish this document to whichever format we like.


<a id="misc-data"></a>

# Misc Data

The following test has nothing to do with the previous content.

```elisp
(concat "<table>" data "</table>")
```

```sqlite
CREATE TABLE IF NOT EXISTS base (
             id DECIMAL PRIMARY KEY,
             name VARCHAR(40),
             age DECIMAL,
             car VARCHAR(40),
             movie VARCHAR(40),
             food VARCHAR(40),
             siblings DECIMAL,
             boss VARCHAR(40)
);
INSERT OR IGNORE INTO base (id, name, age, car, movie, food, siblings, boss)
            VALUES (1, "Bob", 40, "civic", "apollo 13", "cheeseburger", 2, "John"),
                   (2, "Alice", 40, "windstar", "notebook", "salad", 1, "John");
SELECT * FROM base;
```

<table><TR><TH>id</TH>
<TH>name</TH>
<TH>age</TH>
<TH>car</TH>
<TH>movie</TH>
<TH>food</TH>
<TH>siblings</TH>
<TH>boss</TH>
</TR>
<TR><TD>1</TD>
<TD>Bob</TD>
<TD>40</TD>
<TD>civic</TD>
<TD>apollo 13</TD>
<TD>cheeseburger</TD>
<TD>2</TD>
<TD>John</TD>
</TR>
<TR><TD>2</TD>
<TD>Alice</TD>
<TD>40</TD>
<TD>windstar</TD>
<TD>notebook</TD>
<TD>salad</TD>
<TD>1</TD>
<TD>John</TD>
</TR>
</table>
