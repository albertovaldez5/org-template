# [[file:knote.org::#download-the-files][interface]]
import argparse


def main(argc: int, argv: argparse.Namespace):

    if argc == 0:
        print("No arguments.")
        return None


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Generate a redirecting index.html")
    parser.add_argument(
        "path", metavar="path", type=str, help="path to redirect from index.html"
    )
    args = parser.parse_args()
    main(len(args.__dict__), args)
# interface ends here
