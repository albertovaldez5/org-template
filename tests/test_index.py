# [[file:../src/theme.org::#tests][scraper]]
from pathlib import Path
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager


def setup_browser():
    """Start a headless Chrome browser."""
    options = Options()
    options.headless = True
    service = Service(ChromeDriverManager().install())
    return webdriver.Chrome(service=service, options=options)


def get_first(target: str, browser, url: str):
    """Find any first element of tag equal to given target."""
    browser.get(url)
    soup = BeautifulSoup(browser.page_source, "html.parser")
    return soup.find(target)


def test_browser():
    """Check if there is a table in the document and print it"""
    filepath = Path.cwd().parent / Path("../resources/scrapeme.html".replace("../", ""))
    browser = setup_browser()
    table = get_first("table", browser, f"file://{filepath}")
    assert table is not None
    print(f"{table}")
    browser.quit()


if __name__ == "__main__":
    test_browser()
# scraper ends here
